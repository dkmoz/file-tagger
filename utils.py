import magic, os, json
import subprocess as sp

from decimal import Decimal
from ffmpy import FFmpeg
from xdg import BaseDirectory
from xdg import DesktopEntry

class XDGError(Exception): pass

def xdg_query(command, parameter):
    p = sp.Popen(['xdg-mime', 'query', command, parameter],
            stdout=sp.PIPE, stderr=sp.PIPE)
    output, errors = p.communicate()
    if p.returncode or errors:
        raise XDGError('xdg-mime returned error code %d: %s' %
            (p.returncode, errors.strip()))
    return output.strip().decode("utf-8")

# i'm not very proud of this so if you know a better way to open 
# multiple files in default (preferred) application, please let me know
def get_default(filename):
    filetype = magic.from_file(filename, mime=True)
    desktop_filename = xdg_query("default", filetype)

    for dir in BaseDirectory.load_data_paths("applications", desktop_filename):
        return DesktopEntry.DesktopEntry(dir).getExec().split(" ")[0]

def get_video_length(path):
    command = ["ffprobe",
            "-loglevel",  "quiet",
            "-print_format", "json",
             "-show_format", "-show_streams", path ]

    pipe = sp.Popen(command, stdout=sp.PIPE, stderr=sp.STDOUT)
    out, err = pipe.communicate()
    res = json.loads(out)

    seconds = 0
    if 'format' in res:
        if 'duration' in res['format']:
            seconds = float(res['format']['duration'])

    if 'streams' in res:
        for s in res['streams']:
            if 'duration' in s:
                seconds = float(s['duration'])
    return seconds

def loadImages(dirpath, fdata):
        def get_timestamp(seconds):
            m, s = divmod(seconds, 60)
            h, m = divmod(m, 60)
            return "%02d:%02d:%02d" % (h, m, s)

        tmp = fdata
        filepath = tmp[tmp.find(" ") + 1:]
        seconds = get_video_length(filepath)

        tslice = seconds / 10
        for i in range(1, 10):
            tmp = fdata
            filepath = tmp[tmp.find(" ") + 1:]
            n = "/"+ str(i)
            tstamp = get_timestamp(tslice * i)
            ff = FFmpeg(
                inputs={filepath:"-ss " + str(tstamp)},
                outputs={dirpath + n + ".jpg":"-frames:v 1 -y -loglevel -8"})
            ff.run()
            print(dirpath + n + ".jpg created")

