from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui import *

class MyCompleter(QCompleter):
    def __init__(self, *args):
        super(MyCompleter, self).__init__(*args)
        self.local_completion_prefix = ""
        self.source_model = None
        self.filterProxyModel = QSortFilterProxyModel(self)
        self.usingOriginalModel = False

    def setModel(self, model):
        self.source_model = model
        self.filterProxyModel = QSortFilterProxyModel(self)
        self.filterProxyModel.setSourceModel(self.source_model)
        super(MyCompleter, self).setModel(self.filterProxyModel)
        self.usingOriginalModel = True

    def updateModel(self):
        if not self.usingOriginalModel:
            self.filterProxyModel.setSourceModel(self.source_model)

        pattern = QtCore.QRegExp(self.local_completion_prefix,
                                QtCore.Qt.CaseInsensitive,
                                QtCore.QRegExp.FixedString)
        self.filterProxyModel.setFilterRegExp(pattern)

    def splitPath(self, path):
        path = path.split(" ")[-1]
        self.local_completion_prefix = path
        self.updateModel()
        if self.filterProxyModel.rowCount() == 0:
            self.usingOriginalModel = False
            self.filterProxyModel.setSourceModel(QStringListModel([]))
        return []


class MyLineEdit(QLineEdit):
    def __init__(self, SearchForm, fileListModel, fileList, db):
        super(QLineEdit, self).__init__(SearchForm)

        self.fileListModel = fileListModel
        self.fileList = fileList
        self.db = db
        
        self.completions = [] # All available completions
        self.partials = [] # Parsed completions according to partially typed word
        self.cursorpos = 0
        self.wordstart = -1 # Start of word where cursor positioned
        self.wordend = -1 # End of word where cursor is positioned
        self.lastindex = -1 # Last used completion of available completions
        
    def setCompletions(self, items):
        self.completions = items
        
    def getWords(self):
        text = str(self.text())
        
        if self.lastindex == -1:
            self.cursorpos = self.cursorPosition()
            self.wordstart = text.rfind(" ", 0, self.cursorpos) + 1

        pattern = text[self.wordstart:self.cursorpos].lower()
        self.partials = [tag for tag in self.completions if tag.lower().find(pattern) != -1]
        return self.partials
        
    def applyCompletion(self, text):
        old = str(self.text())
        self.wordend = old.find(" ", self.cursorpos)
        
        if self.wordend == -1:
            self.wordend = len(old)
            
        new = old[:self.wordstart] + text + old[self.wordend:]
        self.setText(new)
        
