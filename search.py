# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from CustomLineEdit import *

from tag import Ui_TagForm

import os, subprocess, time
from db import DB
from utils import *
from ffmpy import FFmpeg

VIDEO_PREVIEW_ENABLED = True
VIDEO_THUMBNAIL_CACHING = True

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class TagForm(QtGui.QWidget, Ui_TagForm):
    def __init__(self, files):
        QtGui.QWidget.__init__(self)
        self.setupUi(self, files)

class MyThread(QThread):
    def __init__(self, images):
        QThread.__init__(self)
        self.images = images

    def __del__(self):
        self.wait()

    def run(self):
        for image in self.images:
            self.emit(SIGNAL('change_tn(QString)'), image)
            self.sleep(1)

class MyCustomLineEdit(MyLineEdit):
    def __init__(self, SearchForm, fileListModel, fileList, db):
        return MyLineEdit.__init__(self, SearchForm, fileListModel, fileList, db)

    def event(self, event):
        if event.type() == QEvent.KeyPress:
            if event.key() == Qt.Key_Tab:
                if not len(self.completions):
                    return True
                
                if self.lastindex >= len(self.getWords()) or self.lastindex == -1:
                    self.lastindex = 0
                    
                # only one item in the completion list
                if len(self.partials) == 1:
                    self.applyCompletion(self.partials[0])
                # more than one match
                elif len(self.partials) > 1:
                    match = self.partials[self.lastindex]
                    self.lastindex += 1
                    self.applyCompletion(match)
                return True

            if event.key() == Qt.Key_Return:
                if self.fileListModel.rowCount() > 0:
                    self.fileListModel.removeRows(0, self.fileListModel.rowCount())

                tags = self.text().split(" ")
                files = self.db.searchFile(tags)
                for file in files:
                    item = QStandardItem(QIcon(file[3]), file[2].split('/')[-1])
                    item.setEditable(0)
                    item.setAccessibleDescription(file[1] + " " + file[2])
                    self.fileListModel.appendRow(item)

                self.fileList.setModel(self.fileListModel)
                self.fileList.setCurrentIndex(self.fileListModel.index(0,0))
                self.fileList.show()
                self.fileList.setFocus(True)
            else:
                self.lastindex = -1
        return QLineEdit.event(self, event)

# implement event def for this file and another event def for tag.py

# Custom QListView implementation as we need to modify keyboard behaviour
class MyListView(QListView):
    def __init__(self, SearchForm):
        super(QListView, self).__init__(SearchForm)
        self.index = 0
        self.lineEdit = None

    def getMyLineEdit(self, lineEdit):
        self.lineEdit = lineEdit

    def keyPressEvent(self, event):
        m = QApplication.keyboardModifiers()
        if event.type() == QEvent.KeyPress:
            # remap j to down
            if event.key() == Qt.Key_J:
                event = QKeyEvent(event.type(), Qt.Key_Down, event.modifiers())

            # remap k to up
            elif event.key() == Qt.Key_K:
                event = QKeyEvent(event.type(), Qt.Key_Up, event.modifiers())

            elif (event.key() == Qt.Key_P and m == Qt.ControlModifier) or event.key() == Qt.Key_P:
                if VIDEO_PREVIEW_ENABLED:
                    self.emit(SIGNAL("playVideoPreview()"))
                return True # TODO fix this

            # open window for editing tags
            elif (event.key() == Qt.Key_E and m == Qt.ControlModifier) or event.key() == Qt.Key_E:
                fpath = []
                if len(self.selectedIndexes()) > 1:
                    for counter, index in enumerate(self.selectedIndexes()):
                        tmp = self.model().itemData(index)[12]
                        intersect = tmp.find(" ")
                        fdata = (counter, tmp[0:intersect], tmp[intersect+1:])
                        fpath.append(fdata)
                else:
                    tmp = self.model().itemData(self.currentIndex())[12]
                    intersect = tmp.find(" ")
                    fdata = (0, tmp[0:intersect], tmp[intersect+1:])
                    fpath.append(fdata)

                TagForm = QtGui.QDialog()
                ui = Ui_TagForm()
                ui.setupUi(TagForm, fpath)
                TagForm.show()
                return True # TODO fix this

            elif (event.key() == Qt.Key_F and m == Qt.ControlModifier) or event.key() == Qt.Key_F:
                self.lineEdit.setFocus(True)
                return True # TODO FIX THIS!!!

            # open selected file(s) in external program (preferred program)
            elif (event.key() == Qt.Key_O and m == Qt.ControlModifier) or event.key() == Qt.Key_O:

                fpath = []
                if len(self.selectedIndexes()) > 1:
                    for index in self.selectedIndexes():
                        tmp = self.model().itemData(index)[12]
                        fpath.append(tmp[tmp.find(" ") + 1:])
                else:
                    tmp = self.model().itemData(self.currentIndex())[12]
                    fpath.append(tmp[tmp.find(" ") + 1:])

                if os.name == "posix":
                    defapp = get_default(fpath[0])
                    subprocess.Popen([defapp] + fpath)
                elif os.name == "nt":
                    print("haloust windows user")
                return True # TODO FIX THIS!!!

            return QListView.keyPressEvent(self, event)


class Ui_SearchForm(object):

    def setupUi(self, SearchForm):
        if "FILETAGGER_DB_PATH" in globals():
            self.db = DB(FILETAGGER_DB) # TODO: open file dialog and get db path/name from user
        else:
            self.db = DB("testi.db") # TODO: open file dialog and get db path/name from user
        self.tags = self.db.getTags()
        self.selectedFile = None

        SearchForm.setObjectName(_fromUtf8("SearchForm"))
        SearchForm.setFixedSize(700, 550)

        self.txtTagsModel = QStringListModel()
        self.txtTagsModel.setStringList([tag[1] for tag in self.tags])

        self.completer = MyCompleter()
        self.completer.setCompletionColumn(0)
        self.completer.setModel(self.txtTagsModel)
        self.completer.setCompletionRole(Qt.DisplayRole)
        self.completer.setCaseSensitivity(Qt.CaseInsensitive) 

        self.lstFiles = MyListView(SearchForm)
        self.lstFiles.setGeometry(QtCore.QRect(10, 40, 281, 500))
        self.lstFiles.clicked.connect(self.getFileData)
        self.lstFiles.setObjectName(_fromUtf8("lstFiles"))
        self.lstFiles.setSelectionMode(QAbstractItemView.ExtendedSelection)
        QObject.connect(self.lstFiles, SIGNAL("playVideoPreview()"), self.playVideoPreview)

        self.lstFilesModel = QStandardItemModel(self.lstFiles)
        self.lstFiles.setModel(self.lstFilesModel)
        self.lstFiles.selectionModel().selectionChanged.connect(self.itemSelected)

        self.txtTags = MyCustomLineEdit(SearchForm, self.lstFilesModel, self.lstFiles, self.db)
        # self.txtTags = MyLineEdit(SearchForm, self.lstFilesModel, self.lstFiles, self.db)
        self.txtTags.setCompletions([tag[1] for tag in self.tags])
        self.txtTags.setGeometry(QtCore.QRect(0, 10, 520, 22))
        self.txtTags.setObjectName(_fromUtf8("txtTags"))
        self.txtTags.setCompleter(self.completer)
        self.txtTags.setFocus(True)
        self.txtTags.textChanged.connect(self.txtTagsTextChanged)

        self.lstFiles.getMyLineEdit(self.txtTags)

        self.btnSearch = QtGui.QPushButton(SearchForm)
        self.btnSearch.setGeometry(QtCore.QRect(525, 10, 80, 22))
        self.btnSearch.setObjectName(_fromUtf8("btnSearch"))
        self.btnSearch.setEnabled(0)
        self.btnSearch.clicked.connect(self.btnSearchClicked)

        self.btnOptions = QtGui.QPushButton(SearchForm)
        self.btnOptions.setGeometry(QtCore.QRect(610, 10, 80, 22))
        self.btnOptions.setObjectName(_fromUtf8("btnOptions"))
        self.btnOptions.setEnabled(1)
        self.btnOptions.clicked.connect(self.openOptionsWindow)

        self.btnEdit = QtGui.QPushButton(SearchForm)
        self.btnEdit.setGeometry(QtCore.QRect(390, 515, 80, 22))
        self.btnEdit.setObjectName(_fromUtf8("btnEdit"))
        self.btnEdit.setEnabled(0)
        self.btnEdit.clicked.connect(self.openEditWindow)

        self.btnOpen = QtGui.QPushButton(SearchForm)
        self.btnOpen.setGeometry(QtCore.QRect(300, 515, 80, 22))
        self.btnOpen.setObjectName(_fromUtf8("btnOpen"))
        self.btnOpen.setEnabled(0)
        self.btnOpen.clicked.connect(self.openFileExternal)

        self.frame = QtGui.QLabel(SearchForm)
        self.frame.setGeometry(300, 40, 395, 400)
        self.frame.setScaledContents(True)
        self.frame.mousePressEvent = self.handleMousePressEvent

        self.lstTags = QtGui.QListView(SearchForm)
        self.lstTags.setGeometry(QtCore.QRect(300, 445, 170, 60))
        self.lstTags.setObjectName(_fromUtf8("lstTags"))
        self.lstTagsModel = QStandardItemModel(self.lstTags)

        self.retranslateUi(SearchForm)
        QtCore.QMetaObject.connectSlotsByName(SearchForm)

    def btnSearchClicked(self, event):
        if self.lstFilesModel.rowCount() > 0:
            self.lstFilesModel.removeRows(0, self.lstFilesModel.rowCount())

        tags = self.txtTags.text().split(" ")
        files = self.db.searchFile(tags)
        for file in files:
            item = QStandardItem(QIcon(file[3]), file[2].split('/')[-1])
            item.setEditable(0)
            item.setAccessibleDescription(file[1] + " " + file[2])
            self.lstFilesModel.appendRow(item)

        self.lstFiles.setModel(self.lstFilesModel)
        self.lstFiles.setCurrentIndex(self.lstFilesModel.index(0, 0))
        self.lstFiles.show()
        self.lstFiles.setFocus(True)

    def txtTagsTextChanged(self, event):
        if len(self.txtTags.text()) == 0:
            self.btnSearch.setEnabled(0)
        else:
            self.btnSearch.setEnabled(1)

    def change_tn(self, image):
        self.pixmap = QPixmap(image)
        self.frame.setPixmap(self.pixmap)

    def handleMousePressEvent(self, event):
        self.playVideoPreview()

    def playVideoPreview(self):
        if self.selectedFile != None:
            dirpath = "/tmp/filetagger/" + self.selectedFile[12].split(" ")[0]
            if os.path.exists(dirpath) and VIDEO_PREVIEW_ENABLED:
                path, dirs, files = next(os.walk(dirpath))
                if len(files) != 10:
                    loadImages(path, self.selectedFile[12])
                
                path, dirs, files = next(os.walk(dirpath))
                self.get_thread = MyThread([path + "/" + f for f in reversed(files)])
                QObject.connect(self.get_thread, SIGNAL('change_tn(QString)'), self.change_tn)
                self.get_thread.start()

    def itemSelected(self):
        self.getFileData(self.lstFiles.currentIndex())

    # Open selected file(s) in external program (images in image viewer etc)
    def openFileExternal(self):
        fpath = []
        if len(self.lstFiles.selectedIndexes()) > 1:
            for index in self.lstFiles.selectedIndexes():
                tmp = self.lstFiles.model().itemData(index)[12]
                fpath.append(tmp[tmp.find(" ") + 1:])
        else:
            tmp = self.lstFiles.model().itemData(self.lstFiles.currentIndex())[12]
            fpath.append(tmp[tmp.find(" ") + 1:])

        if os.name == "posix":
            defapp = get_default(fpath[0])
            subprocess.Popen([defapp] + fpath)
        elif os.name == "nt":
            print("haloust windows user")

    def openOptionsWindow(self):
        print("opening options window...")

    def openEditWindow(self):
        fpath = []
        if len(self.lstFiles.selectedIndexes()) > 1:
            for counter, index in enumerate(self.lstFiles.selectedIndexes()):
                tmp = self.lstFiles.model().itemData(index)[12]
                intersect = tmp.find(" ")
                fdata = (counter, tmp[0:intersect], tmp[intersect+1:])
                fpath.append(fdata)
        else:
            tmp = self.lstFiles.model().itemData(self.lstFiles.currentIndex())[12]
            intersect = tmp.find(" ")
            fdata = (0, tmp[0:intersect], tmp[intersect+1:])
            fpath.append(fdata)

        self.tagWindow = TagForm(fpath)
        self.tagWindow.show()

    def getFileData(self, index):
        filedata = self.lstFiles.model().itemData(index)
        self.selectedFile = filedata

        self.btnOpen.setEnabled(1)
        self.btnEdit.setEnabled(1)

        # this is disgusting but it'll have to do
        # until i figure out what's causing the problem
        tmp = ""
        try:
            tmp = filedata[12]
        except:
            pass

        filepath = tmp[tmp.find(" ") + 1:]
        if not os.path.isfile(filepath):
            print("404")
            self.frame.setPixmap(QPixmap())
            if self.lstTagsModel.rowCount() > 0:
                self.lstTagsModel.removeRows(0, self.lstTagsModel.rowCount())
            return

        if magic.from_file(filepath, mime=True).split("/")[0] == "video":
            dirpath = "/tmp/filetagger/"
            if not os.path.exists(dirpath):
                os.mkdir(dirpath)

            dirpath += filedata[12].split(" ")[0]
            if not os.path.exists(dirpath):
                os.mkdir(dirpath)

                ff = FFmpeg(
                        inputs={filepath:"-ss 00:00:00"},
                        outputs={dirpath+"/0.jpg":"-frames:v 1 -y -loglevel -8"})
                ff.run()
            filepath = dirpath + "/0.jpg"

        self.pixmap = QPixmap(filepath)
        self.frame.setPixmap(self.pixmap)

        tags = self.db.getFileTags(filedata[12].split(" ")[0])
        if self.lstTagsModel.rowCount() > 0:
            self.lstTagsModel.removeRows(0, self.lstTagsModel.rowCount())

        for tag in tags:
            item = QStandardItem(tag)
            self.lstTagsModel.appendRow(item)
        self.lstTags.setModel(self.lstTagsModel)
        self.lstTags.show()

    def retranslateUi(self, SearchForm):
        SearchForm.setWindowTitle(_translate("SearchForm", "Search files", None))
        self.btnSearch.setText(_translate("SearchForm", "Search", None))
        self.btnOptions.setText(_translate("SearchForm", "Options", None))
        self.btnEdit.setText(_translate("SearchForm", "Edit tags", None))
        self.btnOpen.setText(_translate("SearchForm", "Open file", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    SearchForm = QtGui.QDialog()
    ui = Ui_SearchForm()
    ui.setupUi(SearchForm)
    SearchForm.show()
    sys.exit(app.exec_())

