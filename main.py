# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main.ui'
#
# Created by: PyQt4 UI code generator 4.12
#
# WARNING! All changes made in this file will be lost!

import sys
from PyQt4 import QtCore, QtGui
from search import Ui_SearchForm
from tag import Ui_TagForm

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class TagForm(QtGui.QWidget, Ui_TagForm):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.setupUi(self)

class SearchForm(QtGui.QWidget, Ui_SearchForm):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.setupUi(self)

class Ui_MainForm(object):
    def setupUi(self, MainForm):
        self.MainForm = MainForm
        MainForm.setObjectName(_fromUtf8("MainForm"))
        MainForm.resize(292, 103)

        self.btnSearch = QtGui.QPushButton(MainForm)
        self.btnSearch.setGeometry(QtCore.QRect(10, 10, 131, 51))
        self.btnSearch.setObjectName(_fromUtf8("btnSearch"))
        self.btnSearch.clicked.connect(self.handleSearchClicked)

        self.btnTag = QtGui.QPushButton(MainForm)
        self.btnTag.setGeometry(QtCore.QRect(150, 10, 131, 51))
        self.btnTag.setObjectName(_fromUtf8("btnTag"))
        self.btnTag.clicked.connect(self.handleTagClicked)
        
        self.tagWindow = None
        self.searchWindow = None

        self.retranslateUi(MainForm)
        QtCore.QMetaObject.connectSlotsByName(MainForm)


    def handleTagClicked(self):
        if self.tagWindow == None:
            self.tagWindow = TagForm()
        self.tagWindow.show()

    def handleSearchClicked(self):
        if self.searchWindow == None:
            self.searchWindow = SearchForm()
        self.searchWindow.show()
        self.close()

    def retranslateUi(self, MainForm):
        MainForm.setWindowTitle(_translate("MainForm", "File tagger", None))
        self.btnSearch.setText(_translate("MainForm", "Search", None))
        self.btnTag.setText(_translate("MainForm", "Tag", None))


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    MainForm = QtGui.QDialog()
    ui = Ui_MainForm()
    ui.setupUi(MainForm)
    MainForm.show()
    sys.exit(app.exec_())

