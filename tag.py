# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'tag.ui'
#
# Created by: PyQt4 UI code generator 4.12
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from CustomLineEdit import *

from db import *
from utils import *

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class MyCustomLineEdit(MyLineEdit):
    def __init__(self, TagForm, fileListModel, fileList, db):
        self.TagForm = TagForm
        self.files = None
        self.currentFile = None
        return MyLineEdit.__init__(self, TagForm, fileListModel, fileList, db)

    def setFiles(self, files):
        self.files = files

    def setCurrentFile(self, currentFile):
        self.currentFile = currentFile

    def event(self, event):
        m = QApplication.keyboardModifiers()
        if event.type() == QEvent.KeyPress:
            if event.key() == Qt.Key_Tab:
                if not len(self.completions):
                    return True
                
                if self.lastindex >= len(self.getWords()) or self.lastindex == -1:
                    self.lastindex = 0
                    
                # only one item in the completion list
                if len(self.partials) == 1:
                    self.applyCompletion(self.partials[0])
                # more than one match
                elif len(self.partials) > 1:
                    match = self.partials[self.lastindex]
                    self.lastindex += 1
                    self.applyCompletion(match)
                return True
            
            # next file
            elif event.key() == Qt.Key_N and m == Qt.ControlModifier:
                if len(self.files) > self.currentFile[0] + 1:
                    self.emit(SIGNAL("selectNext()"))
                return True

            # previous file
            elif event.key() == Qt.Key_P and m == Qt.ControlModifier:
                if self.currentFile[0] > 0:
                    self.emit(SIGNAL("selectPrevious()"))
                return True

            # delete file
            elif event.key() == Qt.Key_D and m == Qt.ControlModifier:
                self.emit(SIGNAL("deleteSelectedFile()"))
                return True

            # open file dialog
            elif event.key() == Qt.Key_O and m == Qt.ControlModifier:
                self.emit(SIGNAL("openFileDialog()"))
                return True
    
            elif event.key() == Qt.Key_Escape:
                self.TagForm.setFocus(1)
                return True

            # if txtTags has multiple tags, add them all. Don't add duplicate tags
            # if txtTags has only one tag and it's in the tags list, delete the tag
            elif event.key() == Qt.Key_Return:
                tags = self.text().split(" ")
                fileTags = self.db.getFileTags(self.db.md5(self.currentFile[2]))

                # file has no tags, it maybe a new file
                if len(fileTags) == 0:
                    self.db.addFile(self.currentFile[2])

                for tag in tags:
                    if tag == "":
                        continue
                    fhash = self.currentFile[1]
                    if fhash == None:
                        fhash = self.db.md5(self.currentFile[2])
                    if tag in fileTags:
                        self.db.deleteTag(fhash, tag)
                    else:
                        self.db.addTag(fhash, tag)


                if self.fileListModel.rowCount() > 0:
                    self.fileListModel.removeRows(0, self.fileListModel.rowCount())

                fileTags = self.db.getFileTags(self.db.md5(self.currentFile[2]))
                for tag in fileTags:
                    item = QStandardItem(tag)
                    item.setEditable(0)
                    self.fileListModel.appendRow(item)

                self.fileList.setModel(self.fileListModel)
                self.fileList.setCurrentIndex(self.fileListModel.index(0,0))
                self.fileList.show()
                self.setText("")
            else:
                self.lastindex = -1
        return QLineEdit.event(self, event)


class Ui_TagForm(object):
    def setupUi(self, TagForm, files):
        self.db = DB("testi.db") # TODO: open file dialog and get db path/name from user
        self.tags = self.db.getTags()

        # build (or get) list of files in format (index, md5 hash, filepath)
        self.files = files
        self.currentFile = self.files[0] if self.files else None

        TagForm.setObjectName(_fromUtf8("TagForm"))
        TagForm.setFixedSize(669, 445)

        self.frame = QtGui.QLabel(TagForm)
        self.frame.setGeometry(5, 5, 400, 431)
        self.frame.setScaledContents(True)
        self.frame.mousePressEvent = self.handleMousePressEvent
        self.frame.setAcceptDrops(True)
        self.frame.dragEnterEvent = self.frameDragEnterEvent
        self.frame.dropEvent = self.frameDropEvent

        self.fileTags = QtGui.QListView(TagForm)
        self.fileTags.setGeometry(QtCore.QRect(410, 10, 256, 291))
        self.fileTags.setObjectName(_fromUtf8("fileTags"))
        self.fileTagsModel = QStandardItemModel(self.fileTags)

        self.txtTagsModel = QStringListModel()
        self.txtTagsModel.setStringList([tag[1] for tag in self.tags])

        self.completer = MyCompleter()
        self.completer.setCompletionColumn(0)
        self.completer.setModel(self.txtTagsModel)
        self.completer.setCompletionRole(Qt.DisplayRole)
        self.completer.setCaseSensitivity(Qt.CaseInsensitive) 

        # FIXME this is absolutely disgusting
        self.txtTags = MyCustomLineEdit(TagForm, self.fileTagsModel, self.fileTags, self.db)
        self.txtTags.setGeometry(QtCore.QRect(410, 310, 151, 25))
        self.txtTags.setObjectName(_fromUtf8("txtTags"))
        self.txtTags.setCompletions([tag[1] for tag in self.tags])
        self.txtTags.setCompleter(self.completer)
        self.txtTags.setFocus(True)

        self.btnTagEvent = QtGui.QPushButton(TagForm)
        self.btnTagEvent.setGeometry(QtCore.QRect(570, 310, 92, 26))
        self.btnTagEvent.setObjectName(_fromUtf8("btnTagEvent"))
        self.btnTagEvent.setEnabled(False)

        self.btnNext = QtGui.QPushButton(TagForm)
        self.btnNext.setGeometry(QtCore.QRect(540, 350, 121, 26))
        self.btnNext.setObjectName(_fromUtf8("btnNext"))
        self.btnNext.clicked.connect(self.selectNext)
        if self.files and len(self.files) == 1:
            self.btnNext.setEnabled(False)

        self.btnPrev = QtGui.QPushButton(TagForm)
        self.btnPrev.setGeometry(QtCore.QRect(410, 350, 121, 26))
        self.btnPrev.setObjectName(_fromUtf8("btnPrev"))
        self.btnPrev.setEnabled(False)
        self.btnPrev.clicked.connect(self.selectPrevious)

        self.btnDelete = QtGui.QPushButton(TagForm)
        self.btnDelete.setGeometry(QtCore.QRect(410, 380, 121, 26))
        self.btnDelete.setObjectName(_fromUtf8("btnDelete"))
        self.btnDelete.clicked.connect(self.deleteSelectedFile)

        # TODO this could not be more useless button
        self.btnSave = QtGui.QPushButton(TagForm)
        self.btnSave.setGeometry(QtCore.QRect(540, 380, 121, 26))
        self.btnSave.setObjectName(_fromUtf8("btnSave"))

        self.btnOpenFiles = QtGui.QPushButton(TagForm)
        self.btnOpenFiles.setGeometry(QtCore.QRect(410, 410, 121, 26))
        self.btnOpenFiles.setObjectName(_fromUtf8("btnOpenFiles"))
        self.btnOpenFiles.clicked.connect(self.openFileDialog)

        self.btnOptions = QtGui.QPushButton(TagForm)
        self.btnOptions.setGeometry(QtCore.QRect(540, 410, 121, 26))
        self.btnOptions.setObjectName(_fromUtf8("btnOptions"))
        self.btnOptions.clicked.connect(self.openOptionsWindow)

        QObject.connect(self.txtTags, SIGNAL("selectNext()"), self.selectNext)
        QObject.connect(self.txtTags, SIGNAL("selectPrevious()"), self.selectPrevious)
        QObject.connect(self.txtTags, SIGNAL("deleteSelectedFile()"), self.deleteSelectedFile)
        QObject.connect(self.txtTags, SIGNAL("openFileDialog()"), self.openFileDialog)

        if self.files:
            self.updateData()
        self.retranslateUi(TagForm)
        QtCore.QMetaObject.connectSlotsByName(TagForm)


    def frameDragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            event.ignore()

    def frameDropEvent(self, event):
        self.files = []

        for index, file in enumerate(event.mimeData().urls()):
            path = file.toLocalFile()
            self.files.append((index, self.db.md5(path), path))
        self.currentFile = self.files[0]
        self.updateData()
        self.txtTags.setFocus(True)
        event.accept()

    # set thumbnail and load file tags
    # enable/disable btnDelete, btnNext and btnPrev
    def updateData(self):
        filepath = self.currentFile[2]
        if magic.from_file(filepath, mime=True).split("/")[0] == "video":
            dirpath = "/tmp/filetagger/"
            if not os.path.exists(dirpath):
                os.mkdir(dirpath)

            dirpath += self.currentFile[1]
            if not os.path.exists(dirpath):
                os.mkdir(dirpath)

                ff = FFmpeg(
                        inputs={filepath:"-ss 00:00:00"},
                        outputs={dirpath+"/0.jpg":"-frames:v 1 -y -loglevel -8"})
                ff.run()
            filepath = dirpath + "/0.jpg"
        self.frame.setPixmap(QPixmap(filepath))

        self.txtTags.setFiles(self.files)
        self.txtTags.setCurrentFile(self.currentFile)

        curfile = self.currentFile[1]
        fhash = self.db.md5(self.currentFile[2]) if curfile == None else curfile
        tags = self.db.getFileTags(fhash)

        # check if file exists in database and enable/disable btnDelete accordingly
        state = True if self.db.getFileID(fhash) else False
        self.btnDelete.setEnabled(state)

        if self.fileTagsModel.rowCount() > 0:
            self.fileTagsModel.removeRows(0, self.fileTagsModel.rowCount())

        for tag in tags:
            item = QStandardItem(tag)
            self.fileTagsModel.appendRow(item)
        self.fileTags.setModel(self.fileTagsModel)
        self.fileTags.show()

        self.txtTags.setFocus(True)
        self.btnNext.setEnabled(False if len(self.files) == self.currentFile[0] + 1 else True)
        self.btnPrev.setEnabled(True if self.currentFile[0] > 0 else False)

    def selectNext(self):
        self.currentFile = self.files[self.currentFile[0] + 1]
        self.txtTags.setCurrentFile(self.currentFile)
        self.updateData()

    def selectPrevious(self):
        self.currentFile = self.files[self.currentFile[0] - 1]
        self.txtTags.setCurrentFile(self.currentFile)
        self.updateData()

    def deleteSelectedFile(self):
        fhash = self.currentFile[1]
        if fhash == None:
            fhash = self.db.md5(self.currentFile[2])
        self.db.deleteFile(fhash)

        self.files.remove(self.currentFile)
        print(self.files)
        self.txtTags.setFiles(self.files)

        if self.currentFile[0] != 0:
            self.selectPrevious()
        elif len(self.files) > self.currentFile[0] + 1:
            self.selectNext()
        else:
            self.frame.setPixmap(QPixmap())
            if self.fileTagsModel.rowCount() > 0:
                self.fileTagsModel.removeRows(0, self.fileTagsModel.rowCount())

    # def event(self, event):
    #     print("here")
    #     m = QApplication.keyboardModifiers()
    #     if event.type() == QEvent.KeyPress:
    #         if event.key() == Qt.Key_N:
    #             self.selectNext()
    #         elif event.key() == Qt.Key_P:
    #             self.selectPrevious()
    #         elif event.key() == Qt.Key_F:
    #             self.txtTags.setFocus(1)
    #         elif event.key() == Qt.Key_D:
    #             self.deleteSelectedFile()

    def openOptionsWindow(self):
        print("opening options window...")
        pass

    def openFileDialog(self):
        self.files = QFileDialog.getOpenFileNames(None, "Select files")
        self.files = [(index, self.db.md5(file), file) for index, file in enumerate(self.files)]

        for file in self.files:
            print(file)
        self.currentFile = self.files[0]
        self.updateData()

    def handleMousePressEvent(self, event):
        print("vou")

    def retranslateUi(self, TagForm):
        TagForm.setWindowTitle(_translate("TagForm", "Dialog", None))
        self.btnTagEvent.setText(_translate("TagForm", "...", None))
        self.btnNext.setText(_translate("TagForm", "Next file", None))
        self.btnPrev.setText(_translate("TagForm", "Previous file", None))
        self.btnDelete.setText(_translate("TagForm", "Delete file", None))
        self.btnSave.setText(_translate("TagForm", "Save changes", None))
        self.btnOpenFiles.setText(_translate("TagForm", "Open files...", None))
        self.btnOptions.setText(_translate("TagForm", "Options", None))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    TagForm = QtGui.QDialog()
    ui = Ui_TagForm()
    ui.setupUi(TagForm, None)
    TagForm.show()
    sys.exit(app.exec_())

