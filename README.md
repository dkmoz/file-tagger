# file-tagger

![](output.gif)

file-tagger was originally a very simple command line tool with a handful of commands. I guess it was "too simple" because I never used it. Maybe a nice GUI will give me enough motivation to tag my files.

file-tagger has both normal controls (mouse, ctrl+key) for normal people and vim keybindings for special people.

The development is currently on hold. file-tagger is in working condition and you can tag your files with it but there are still a lot of features missing. Hang tight!

Icons made by [Madebyoliver](http://www.flaticon.com/authors/madebyoliver) from [http://www.flaticon.com](http://www.flaticon.com) is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/)

# Features
* Image and video thumbnails
* Video preview (if enabled)
* Open files in external programs
* Vim keybindings
* Flexible file editing with simple shortcuts

# todo list
* how to handle file relocation?!? (is that even our problem?)
* add options window and write options to config file
* implement working multiselect (for editing and opening)
* add video thumbnail caching (enabled/disabled in options)
* file drag-and-drop in editing window
* calculate file hash only when it's selected in tag.py (now the ui hangs..)

# Requirements
### Python modules
* [PyQt4](https://pypi.python.org/pypi/PyQt4/4.11.4)
* [python-magic](https://pypi.python.org/pypi/magic/0.1)
* [ffmpy](https://pypi.python.org/pypi/FFVideo)
* [PyXDG (for linux users)](http://freedesktop.org/wiki/Software/pyxdg)

### External programs
* ffmpeg (if video preview is enabled)
* ffprobe (if video preview is enabled)

# How to use

## Browsing shortcuts

| Shortcut                          | Action                           |
| --------------------------------- | -------------------------------- |
| j or arrow down                   | move down in the file list       |
| k or arrow up                     | move up in the file list         |
| (ctrl+)o or Open file button      | open file(s) in external program |
| (ctrl+)e or Edit tags button      | open file(s) in editing window   |
| (ctrl+)f                          | set focus to search bar          |
| (ctrl+)p or click video thumbnail | play (TODO toggle) video preview |

## Editing shortcuts

| Shortcut              | Action                           |
| --------------------- | -------------------------------- |
| ctrl+n or k  TODO!!!  | select next file                 |
| ctrl+p or j  TODO!!!  | select previous file             |
| (ctrl+)d     TODO!!!  | delete current file from         |
| (ctrl+)o     TODO!!!  | open file dialog                 |


# Copying
file-tagger is free software. It's licensed under the MIT License.
