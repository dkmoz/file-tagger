#>!/usr/bin/env python
# encoding: utf-8

import sqlite3, argparse, os, hashlib, magic

class DB:
    def __init__(self, dbfile):
        self.conn = sqlite3.connect(dbfile)
        self.cur = self.conn.cursor()
        pass

    def md5(self, fname):
        hash_md5 = hashlib.md5()
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()

    def getTags(self):
        c = self.cur.execute("SELECT * FROM tags")
        return self.cur.fetchall()

    def getFileTags(self, filehash):
        q =  "SELECT name FROM tags INNER JOIN filetags ON filetags.tid = tags.tid"
        q += " INNER JOIN files ON filetags.fid = files.fid WHERE files.hash=?"
        c = self.cur.execute(q, (filehash,))
        return [tag[0] for tag in self.cur.fetchall()]

    def createBD(self, dbname):
        conn = sqlite3.connect(os.path.abspath(dbname))
        c = conn.cursor()
        c.execute("CREATE TABLE filetags (fid INT, tid INT)")
        c.execute("CREATE TABLE tags (tid INTEGER PRIMARY"
                + " KEY AUTOINCREMENT, name TEXT)")
        c.execute("CREATE TABLE files (fid INTEGER PRIMARY "
                + "KEY AUTOINCREMENT, path TEXT, hash TEXT, icon TEXT)")
        conn.commit()
        conn.close()

    def getFileID(self, fhash):
        q = "SELECT fid FROM files WHERE hash = ?"
        self.cur.execute(q, (fhash,))
        return self.cur.fetchall()

    # caller checks the return value's validity
    def getTagID(self, tag):
        q = "SELECT tid FROM tags WHERE name = ?"
        self.cur.execute(q, (tag,))
        return self.cur.fetchall()

    def deleteFile(self, fhash):
        fileID = self.getFileID(fhash)
        if fileID:
            self.cur.execute("DELETE FROM files WHERE fid = ?", (fileID[0][0],))
            self.cur.execute("DELETE FROM filetags WHERE fid = ?", (fileID[0][0],))
            self.conn.commit()
        
    def addTag(self, fhash, tag):
        fid = self.getFileID(fhash)[0][0]
        tid = self.getTagID(tag)

        if not tid:
            self.cur.execute("INSERT INTO tags ('tid', 'name') VALUES (NULL, ?)", (tag,))
            tid = self.getTagID(tag)

        q = "INSERT INTO filetags ('fid', 'tid') VALUES (?, ?)"
        self.cur.execute(q, (fid, tid[0][0]))
        self.conn.commit()

    def deleteTag(self, fhash, tag):
        fid = self.getFileID(fhash)[0][0]
        tid = self.getTagID(tag)[0][0]

        q = "DELETE FROM filetags WHERE fid = ? and tid = ?"
        self.cur.execute(q, (fid,tid))
        self.conn.commit()

    def searchFile(self, tags):
        q = "SELECT ft.fid, hash, path, icon FROM files ft INNER JOIN filetags ftt on ft.fid = ftt.fid"
        q += " INNER JOIN tags tt ON ftt.tid = tt.tid WHERE tt.name IN (?"
        q += ", ?" * (len(tags) - 1)
        q += ") group by ft.hash having count(distinct tt.name) = " + str(len(tags))

        self.cur.execute(q, tuple(tags))
        return [file for file in self.cur.fetchall()]

    def getIconPath(self, infile):
        icons = [('image', 'icons/image.png'), ('video', 'icons/video'), 
                ('audio', 'icons/audio.png')]
        ftype = magic.from_file(infile, mime=True).split('/')[0]
        for type, path in icons:
            if type == ftype:
                return path
        return 'icons/misc.png'

    # add new file to database if it doesn't exist
    def addFile(self, path):
        fhash = self.md5(path)
        fname = os.path.abspath(path)
        ficon = self.getIconPath(path)
        exists = self.cur.execute("SELECT * FROM files WHERE hash = ?", (fhash,)).fetchall()
        
        if not exists:
            self.cur.execute("INSERT INTO files VALUES (NULL, ?, ?, ?)", (fname, fhash, ficon))
